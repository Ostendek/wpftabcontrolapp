﻿using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfTabControlApp
{
    class MainViewModel : INotifyPropertyChanged
    {
        public MainViewModel()
        {
            this.Items = new ObservableCollection<TabItemViewModel>(
                Enumerable.Range(1, 5).Select(
                    i => new TabItemViewModel("Tab item header " + i, "Content of the tab item " + i, this.CloseItem)));

            this.AddCommand = new RelayCommand(AddItem);
        }

        public ObservableCollection<TabItemViewModel> Items { get; private set; }

        public TabItemViewModel SelectedItem { get; set; }


        public RelayCommand AddCommand { get; set; }

        private void AddItem()
        {
            var nextIndex = this.Items.Count + 1;
            var newItem = new TabItemViewModel("Added tab item " + nextIndex, "Content of the tab item " + nextIndex, this.CloseItem);
            this.Items.Add(newItem);
        }

        private void CloseItem(TabItemViewModel CurrentTab)
        {
            this.Items.Remove(CurrentTab);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string propertyName)
        {
            this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
