﻿using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfTabControlApp
{
    class TabItemViewModel
    {
        public TabItemViewModel(string header, string content, Action<TabItemViewModel> onClose)
        {
            this.Header = header;
            this.Content = content;
            this.CloseCommand = new RelayCommand(() => onClose(this));
        }
        public string Header { get; set; }
        public string Content { get; set; }

        public RelayCommand CloseCommand { get; set; }
    }
}

