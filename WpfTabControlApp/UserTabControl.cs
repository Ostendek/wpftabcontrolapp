﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Windows.Controls.Primitives;
using System.Collections.Specialized;

namespace WpfTabControlApp
{
    public class UserTabControl : TabControl
    {
        private Button tabAddItemButton;

        private Panel tabPanelTop;

        static UserTabControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(UserTabControl), new FrameworkPropertyMetadata(typeof(UserTabControl)));
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();


            this.tabPanelTop = GetTemplateChild("HeaderPanel") as Panel;
            this.tabAddItemButton = GetTemplateChild("TabAddItemButton") as Button;


            if (this.tabAddItemButton != null)
            {
                this.tabAddItemButton.Click += tabAddItemButton_Click;
                this.tabAddItemButton.Visibility = this.isAddItemEnabled ? Visibility.Visible : Visibility.Collapsed;
            }
        }


        private bool isAddItemEnabled;

        public bool IsAddItemEnabled
        {
            get { return isAddItemEnabled; }
            set
            {
                isAddItemEnabled = value;

                if (this.tabAddItemButton != null)
                    this.tabAddItemButton.Visibility = isAddItemEnabled ? Visibility.Visible : Visibility.Collapsed;
            }
        }


        public ICommand AddItemCommand
        {
            get { return (ICommand)GetValue(AddItemCommandProperty); }
            set { SetValue(AddItemCommandProperty, value); }
        }

        // Using a DependencyProperty as the backing store for AddItemCommand.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AddItemCommandProperty =
            DependencyProperty.Register("AddItemCommand", typeof(ICommand), typeof(UserTabControl), new PropertyMetadata(null));


        private void tabAddItemButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.AddItemCommand != null && this.AddItemCommand.CanExecute(null))
                this.AddItemCommand.Execute(null);
        }
    
    }
}
